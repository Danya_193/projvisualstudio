﻿using System;


namespace _2048_Game
{
    class Control
    {
        public string score;
        public string text;
        public bool[,] stack = new bool[4, 4];
        public bool can_spawn = false;
        public int[,] num_matrix = new int[4, 4];
        public void KeysDown()
        {
            for (int i = 0; num_matrix.GetLength(1) > i; i++)
            {
                for (int j = num_matrix.GetLength(0) - 1; j >= 0; j--)
                {
                    if (num_matrix[i, j] != 0 && num_matrix[i, j] != null)
                    {
                        for (int k = j + 1; k < num_matrix.GetLength(0); k++)
                        {
                            if (num_matrix[i, k] == 0 || num_matrix[i, k] == null)
                            {
                                num_matrix[i, k] = num_matrix[i, k - 1];
                                num_matrix[i, k - 1] = 0;
                                stack[i, k] = stack[i, k - 1];
                                stack[i, k - 1] = false;
                                can_spawn = true;
                            }
                            if (num_matrix[i, k] == num_matrix[i, k - 1] && !stack[i, k] && !stack[i, k - 1])
                            {
                                num_matrix[i, k] *= 2;
                                num_matrix[i, k - 1] = 0;
                                stack[i, k] = true;
                                score = (Convert.ToInt64(score) + num_matrix[i, k]).ToString();
                                MainForm.mf.ScoreSetInLabel();
                                can_spawn = true;
                            }
                        }
                    }
                }
            }
        }
        public void KeysLeft()
        {
            for (int j = 0; num_matrix.GetLength(1) > j; j++)
            {
                for (int i = 0; num_matrix.GetLength(0) > i; i++)
                {
                    if (num_matrix[i, j] != 0 && num_matrix[i, j] != null)
                    {
                        for (int k = i - 1; k >= 0; k--)
                        {
                            if (num_matrix[k, j] == 0 || num_matrix[k, j] == null)
                            {
                                num_matrix[k, j] = num_matrix[k + 1, j];
                                num_matrix[k + 1, j] = 0;
                                stack[k, j] = stack[k + 1, j];
                                stack[k + 1, j] = false;
                                can_spawn = true;
                            }
                            if (num_matrix[k, j] == num_matrix[k + 1, j] && !stack[k, j] && !stack[k + 1, j])
                            {
                                num_matrix[k, j] *= 2;
                                num_matrix[k + 1, j] = 0;
                                stack[k, j] = true;
                                score = (Convert.ToInt64(score) + num_matrix[i, k]).ToString();
                                MainForm.mf.ScoreSetInLabel();
                                can_spawn = true;
                            }
                        }
                    }
                }
            }
        }
        public void KeysUp()
        {
            for (int i = 0; num_matrix.GetLength(1) > i; i++)
            {
                for (int j = 0; num_matrix.GetLength(0) > j; j++)
                {
                    if (num_matrix[i, j] != 0 && num_matrix[i, j] != null)
                    {
                        for (int k = j - 1; k >= 0; k--)
                        {
                            if (num_matrix[i, k] == 0 || num_matrix[i, k] == null)
                            {
                                num_matrix[i, k] = num_matrix[i, k + 1];
                                num_matrix[i, k + 1] = 0;
                                stack[i, k] = stack[i, k + 1];
                                stack[i, k + 1] = false;
                                can_spawn = true;
                            }
                            if (num_matrix[i, k] == num_matrix[i, k + 1] && !stack[i, k] && !stack[i, k + 1])
                            {
                                num_matrix[i, k] *= 2;
                                num_matrix[i, k + 1] = 0;
                                stack[i, k] = true;
                                score = (Convert.ToInt64(score) + num_matrix[i, k]).ToString();
                                MainForm.mf.ScoreSetInLabel();
                                can_spawn = true;
                            }
                        }
                    }
                }
            }
        }
        public void KeysRight()
        {
            for (int j = 0; num_matrix.GetLength(1) > j; j++)
            {
                for (int i = num_matrix.GetLength(0) - 1; i >= 0; i--)
                {
                    if (num_matrix[i, j] != 0 && num_matrix[i, j] != null)
                    {
                        for (int k = i + 1; k < num_matrix.GetLength(0); k++)
                        {
                            if (num_matrix[k, j] == 0 || num_matrix[k, j] == null)
                            {
                                num_matrix[k, j] = num_matrix[k - 1, j];
                                num_matrix[k - 1, j] = 0;
                                stack[k, j] = stack[k - 1, j];
                                stack[k - 1, j] = false;
                                can_spawn = true;
                            }
                            if (num_matrix[k, j] == num_matrix[k - 1, j] && !stack[k, j] && !stack[k - 1, j])
                            {
                                num_matrix[k, j] *= 2;
                                num_matrix[k - 1, j] = 0;
                                stack[k, j] = true;
                                score = (Convert.ToInt64(score) + num_matrix[i, k]).ToString();
                                MainForm.mf.ScoreSetInLabel();
                                can_spawn = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
