﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace _2048_Game
{
    public partial class MainForm : Form
    {
        public static MainForm mf; 
        Control control;
        PictureBox background = new PictureBox();
        Random random = new Random();
        int[] spawn_num = { 2, 2, 2, 4, 2, 2, 4, 2, 2, 2 };
        List<PictureBox> picture_matrix = new List<PictureBox>();
        PictureBox[,] num_pictures = new PictureBox[4, 4];
        public MainForm()
        {
            InitializeComponent();
            mf = this;
            control = new Control();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            background = new PictureBox();
            this.Focus();
            // Чтение файла с сохранением
            try
            {
                using (FileStream fs = File.OpenRead("BestScore.txt"))
                {
                    // Преобразуем строку в байты
                    byte[] array = new byte[fs.Length];
                    // Считываем данные
                    fs.Read(array, 0, array.Length);
                    // Декодируем байты в строку
                    control.text = System.Text.Encoding.Default.GetString(array);
                    bestscore.Text = control.text;
                }
            }
            catch { }
            background.Location = new Point(0, 40);
            background.Size = new Size(278, 278);
            background.Name = "bk";
            background.TabIndex = 0;
            background.TabStop = false;
            background.BackgroundImage = Properties.Resources.background;
            this.Controls.Add(background);
        }
        public void Stack()
        {
            for (int i = 0; i < control.stack.GetLength(1); i++)
            {
                for (int j = 0; j < control.stack.GetLength(1); j++)
                {
                    control.stack[i, j] = false;
                }
            }
        }
        public void SpawnNumbers()
        {
            List<int> unuse = new List<int>();
            bool tr = false;
            while (!tr)
            {
                for (int i = 0; i < control.num_matrix.GetLength(1) && !tr; i++)
                {
                    for (int j = 0; j < control.num_matrix.GetLength(0) && !tr; j++)
                    {
                        if (control.num_matrix[i, j] == 0 || control.num_matrix[i, j] == null)
                        {
                            int[] randnum = { 0, 0, 1, 0, 0, 1, 0, 0, 0, 0 };
                            int rr = randnum[random.Next(randnum.Length - 1)];
                            if (rr == 1)
                            {
                                int rr1 = spawn_num[random.Next(spawn_num.Length - 1)];
                                PictureBox pb = new PictureBox();
                                pb.Location = new System.Drawing.Point(i * 57 + (i + 1) * 10, 40 + (j * 57 + (j + 1) * 10));
                                pb.Size = new System.Drawing.Size(57, 57);
                                pb.Name = "pb" + picture_matrix.Count + 1;
                                pb.TabIndex = picture_matrix.Count + 1;
                                pb.TabStop = false;
                                if (rr1 == 2)
                                {
                                    pb.BackgroundImage = Properties.Resources._2;
                                    control.num_matrix[i, j] = 2;
                                }
                                else
                                {
                                    pb.BackgroundImage = Properties.Resources._4;
                                    control.num_matrix[i, j] = 4;
                                }
                                this.Controls.Add(pb);
                                pb.BringToFront();
                                picture_matrix.Add(pb);
                                tr = true;
                            }
                        }
                    }
                }
            }
        }
        public void Draw()
        {
            if (!GameOwer.Visible)
            {
                foreach (PictureBox i in picture_matrix)
                {
                    this.Controls.Remove(i);
                }
                picture_matrix = new List<PictureBox>();
                for (int i = 0; i < control.num_matrix.GetLength(1); i++)
                {
                    for (int j = 0; j < control.num_matrix.GetLength(0); j++)
                    {
                        if (control.num_matrix[i, j] != 0 && control.num_matrix[i, j] != null)
                        {
                            PictureBox pb = new PictureBox();
                            pb.Location = new Point(i * 57 + (i + 1) * 10, 40 + (j * 57 + (j + 1) * 10));
                            pb.Size = new Size(57, 57);
                            pb.Name = "pb" + picture_matrix.Count + 1;
                            pb.TabIndex = picture_matrix.Count + 1;
                            pb.TabStop = false;
                            switch (control.num_matrix[i, j])
                            {
                                case 2: pb.BackgroundImage = Properties.Resources._2; break;
                                case 4: pb.BackgroundImage = Properties.Resources._4; break;
                                case 8: pb.BackgroundImage = Properties.Resources._8; break;
                                case 16: pb.BackgroundImage = Properties.Resources._16; break;
                                case 32: pb.BackgroundImage = Properties.Resources._32; break;
                                case 64: pb.BackgroundImage = Properties.Resources._64; break;
                                case 128: pb.BackgroundImage = Properties.Resources._128; break;
                                case 256: pb.BackgroundImage = Properties.Resources._256; break;
                                case 512: pb.BackgroundImage = Properties.Resources._512; break;
                                case 1024: pb.BackgroundImage = Properties.Resources._1024; break;
                                case 2048: pb.BackgroundImage = Properties.Resources._2048; break;
                            }
                            this.Controls.Add(pb);
                            pb.BringToFront();
                            picture_matrix.Add(pb);
                        }
                    }
                }
            }
        }
        private void K_D(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                start_Click_1(null, null);
            }
            if (!GameOwer.Visible)
            {
                switch (e.KeyCode)
                {
                    // Стрелки
                    case Keys.Left: control.KeysLeft(); break;
                    case Keys.Right: control.KeysRight(); break;
                    case Keys.Down: control.KeysDown(); break;
                    case Keys.Up: control.KeysUp(); break;
                    // Кнопки
                    case Keys.A: control.KeysLeft(); break;
                    case Keys.D: control.KeysRight(); break;
                    case Keys.S: control.KeysDown(); break;
                    case Keys.W: control.KeysUp(); break;
                    // Кнопки
                    case Keys.NumPad4: control.KeysLeft(); break;
                    case Keys.NumPad6: control.KeysRight(); break;
                    case Keys.NumPad2: control.KeysDown(); break;
                    case Keys.NumPad8: control.KeysUp(); break;
                }
                Stack();
                if (control.can_spawn)
                {
                    SpawnNumbers();
                }
                else
                {
                    IFGameOwer();
                }
                Draw();
                control.can_spawn = false;
            }
        }

        private void start_Click_1(object sender, EventArgs e)
        {
            scores.Text = "0";
            control.score = null;
            UpDate.Start();
            if (GameOwer.Visible)
            {
                Form1_Load(sender, e);
            }
            GameOwer.Visible = false;
            foreach (PictureBox i in picture_matrix)
            {
                this.Controls.Remove(i);
            }
            control.num_matrix = new int[4, 4];
            picture_matrix = new List<PictureBox>();
            SpawnNumbers();
            SpawnNumbers();
        }
        public void IFGameOwer()
        {
            bool tr = false;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (control.num_matrix[i, j] == 0)
                    {
                        tr = true;
                    }
                }
            }
            if (!tr)
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        try
                        {
                            if (control.num_matrix[i, j] == control.num_matrix[i, j + 1] || control.num_matrix[i, j] == control.num_matrix[i + 1, j])
                            {
                                tr = true;
                            }
                        }
                        catch { }
                    }
                }
                if (!tr)
                {
                    foreach (PictureBox i in picture_matrix)
                    {
                        this.Controls.Remove(i);
                    }
                    UpDate.Stop();
                    this.Controls.Remove(background);
                    GameOwer.BringToFront();
                    if (Int64.Parse(scores.Text) > Int64.Parse(bestscore.Text))
                    {
                        control.score = scores.Text.ToString();
                        bestscore.Text = scores.Text;
                        scores.Text = "0";
                        //Запись в файл
                        using (FileStream fs = new FileStream("BestScore.txt", FileMode.OpenOrCreate))
                        {
                            // Преобразуем строку в байты
                            byte[] array = System.Text.Encoding.Default.GetBytes(control.score);
                            // Запись массива байтов в файл
                            fs.Write(array, 0, array.Length);
                        }
                    }
                    GameOwer.Visible = true;
                }
            }
        }
        public void ScoreSetInLabel()
        {
            scores.Text = control.score;
        }
    }
}
