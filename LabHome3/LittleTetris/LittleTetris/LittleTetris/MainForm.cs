﻿using System;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;

using LittleTetris;
namespace LittleTetris{
    public partial class MainForm : Form{
        SetShape SetShape;
        
        
        public const int width = 15, height = 25, k = 15;
        public Bitmap bitfield = new Bitmap(k * (width + 1) + 1, k * (height + 3) + 1);        
        public int[,] field = new int[MainForm.width, MainForm.height];

        int CountPoints = 0;
        public MainForm(){
            InitializeComponent();
            SetShape = new SetShape();
            SetShape.SetShapes();

        }
        public void FillFields()
        {
            var gr = Graphics.FromImage(bitfield);
            gr.Clear(Color.Black);
            gr.DrawRectangle(Pens.Red, k, k, (width - 1) * k, (height - 1) * k);
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    gr.FillRectangle(Brushes.Green, i * k, j * k, (k - 1) * field[i, j], (k - 1) * field[i, j]);
            for (int i = 0; i < 4; i++)
                gr.FillRectangle(Brushes.Red, SetShape.shape[1, i] * k, SetShape.shape[0, i] * k, k - 1, k - 1);
            FieldPictureBox.Image = bitfield;
        }
        private void TickTimer_Tick(object sender, EventArgs e){
            if (field[8, 4] == 1)
                Environment.Exit(0);
            foreach (int i in (from i in Enumerable.Range(0, field.GetLength(1)) where (Enumerable.Range(0, field.GetLength(0)).Select(j => field[j, i]).Sum() >= width - 1) select i).ToArray().Take(1))
            {
                for (int k = i; k > 1; k--)
                {
                    for (int l = 1; l < width; l++)
                        field[l, k] = field[l, k - 1];
                }
                CountPoints = CountPoints + 5;
                CountPoint.Text = "Очков: " + CountPoints;
            }
            Move(0, 1);
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e){
           switch (e.KeyCode){
            case Keys.Left: Move(-1, 0); break;
            case Keys.Right: Move(1, 0); break; 
            case Keys.Up:               
                var shapeT = new int[2, 4];
                Array.Copy(SetShape.shape, shapeT, SetShape.shape.Length);
                int maxx = Enumerable.Range(0, 4).Select(j => SetShape.shape[1, j]).ToArray().Max();
                int maxy = Enumerable.Range(0, 4).Select(j => SetShape.shape[0, j]).ToArray().Max();
                for (int i = 0; i < 4; i++) { 
                    int temp = SetShape.shape[0, i];
                        SetShape.shape[0, i] = maxy - (maxx - SetShape.shape[1, i]) - 1;
                        SetShape.shape[1, i] = maxx - (3 - (maxy - temp)) + 1;
                }
                if (FindMistakes())
                    Array.Copy(shapeT, SetShape.shape, SetShape.shape.Length);
                break;
            case Keys.Down: TickTimer.Interval = 50; break;
           }
        }      
        private void Form1_KeyUp(object sender, KeyEventArgs e) => TickTimer.Interval = 250;

        public new void Move(int x, int y)
        {
            for (int i = 0; i < 4; i++)
            {
                SetShape.shape[1, i] += x;
                SetShape.shape[0, i] += y;
            }
            if (FindMistakes())
            {
                Move(-x, -y);
                if (y != 0)
                {
                    for (int i = 0; i < 4; i++)
                        field[SetShape.shape[1, i], SetShape.shape[0, i]]++;
                    SetShape.SetShapes();
                }
            }
            FillFields();
        }
        public bool FindMistakes()
        {

            for (int i = 0; i < 4; i++)
                if (SetShape.shape[1, i] >= width || SetShape.shape[0, i] >= height || SetShape.shape[1, i] <= 0 || SetShape.shape[0, i] <= 0 || field[SetShape.shape[1, i], SetShape.shape[0, i]] == 1)
                    return true;
            return false;
        }
    }
}