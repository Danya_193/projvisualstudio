﻿namespace LabHome
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.chbStandart = new System.Windows.Forms.CheckBox();
            this.chListArena = new System.Windows.Forms.CheckedListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.tbTabel = new System.Windows.Forms.TextBox();
            this.tb1 = new System.Windows.Forms.TextBox();
            this.btSummary = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.закрытьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(13, 28);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.DateTimePicker1_ValueChanged);
            // 
            // chbStandart
            // 
            this.chbStandart.AutoSize = true;
            this.chbStandart.Location = new System.Drawing.Point(12, 67);
            this.chbStandart.Name = "chbStandart";
            this.chbStandart.Size = new System.Drawing.Size(73, 17);
            this.chbStandart.TabIndex = 5;
            this.chbStandart.Text = "Стандарт";
            this.chbStandart.UseVisualStyleBackColor = true;
            this.chbStandart.CheckedChanged += new System.EventHandler(this.ChbStandart_CheckedChanged);
            // 
            // chListArena
            // 
            this.chListArena.FormattingEnabled = true;
            this.chListArena.Items.AddRange(new object[] {
            "Арена1",
            "Арена 2",
            "Арена 3"});
            this.chListArena.Location = new System.Drawing.Point(12, 134);
            this.chListArena.Name = "chListArena";
            this.chListArena.Size = new System.Drawing.Size(120, 94);
            this.chListArena.TabIndex = 6;
            this.chListArena.SelectedIndexChanged += new System.EventHandler(this.ChListArena_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 245);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(51, 94);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 9;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.NumericUpDown1_ValueChanged);
            // 
            // tbTabel
            // 
            this.tbTabel.Location = new System.Drawing.Point(12, 283);
            this.tbTabel.Multiline = true;
            this.tbTabel.Name = "tbTabel";
            this.tbTabel.Size = new System.Drawing.Size(251, 99);
            this.tbTabel.TabIndex = 10;
            this.tbTabel.TextChanged += new System.EventHandler(this.TbTabel_TextChanged);
            // 
            // tb1
            // 
            this.tb1.Location = new System.Drawing.Point(258, 96);
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(100, 20);
            this.tb1.TabIndex = 11;
            this.tb1.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // btSummary
            // 
            this.btSummary.Location = new System.Drawing.Point(177, 93);
            this.btSummary.Name = "btSummary";
            this.btSummary.Size = new System.Drawing.Size(75, 23);
            this.btSummary.TabIndex = 12;
            this.btSummary.Text = "Сумма";
            this.btSummary.UseVisualStyleBackColor = true;
            this.btSummary.Click += new System.EventHandler(this.BtSummary_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Часы";
            // 
            // закрытьToolStripMenuItem
            // 
            this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
            this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.закрытьToolStripMenuItem.Text = "Закрыть";
            this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.ЗакрытьToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btSummary);
            this.Controls.Add(this.tb1);
            this.Controls.Add(this.tbTabel);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chListArena);
            this.Controls.Add(this.chbStandart);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox chbStandart;
        private System.Windows.Forms.CheckedListBox chListArena;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.TextBox tbTabel;
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.Button btSummary;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem;
    }
}

